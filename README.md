# README #

JavaScriptで作られたPCブラウザ向けタイピングゲームです。
Web AnimationsというAPIを利用しているので、Google ChromeとFirefoxでしか動きません。
(Chromeを推奨します)

いずれ対戦・協力プレイをできるようにしたいと思っています。

# Emoji Prefix #

このプロジェクトはEmojiPrefixを採用しています コミットメッセージの前に絵文字をつけることで コミットの内容がわかりやすいようにしています
✨新機能 👍機能の改善 ♻️リファクタリング 🐛不具合修正 🚀記念すべきことなど
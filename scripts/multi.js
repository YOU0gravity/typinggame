/*
	<link rel="stylesheet" type="text/css" href="http://afternoon-plateau-83224.herokuapp.com/styles/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="http://afternoon-plateau-83224.herokuapp.com/scripts/jquery.particleground.js"></script>

	<!-- 問題データと敵出現データ -->
	<script type="text/javascript" src="http://afternoon-plateau-83224.herokuapp.com/data/questions.js"></script>
	<script type="text/javascript" src="http://afternoon-plateau-83224.herokuapp.com/data/spawnInfo.js"></script>

	<!-- ゲーム全般に関する処理はここで行う -->
	<script type="text/javascript" src="http://afternoon-plateau-83224.herokuapp.com/scripts/main.js"></script>
	<!-- 副作用のない関数をまとめたもの -->
	<script type="text/javascript" src="http://afternoon-plateau-83224.herokuapp.com/scripts/tools.js"></script>
	<!-- SVG画像を作り出す関数をまとめたもの -->
	<script type="text/javascript" src="http://afternoon-plateau-83224.herokuapp.com/scripts/createSvg.js"></script>

	<script src="/socket.io/socket.io.js"></script>
*/

class MultiMode {
	constructor(){
		this.socket = io();
		this.socket.on('getMyId', function(id){
			gc.myPlayerId = id;
			
			for(let i = 0; i <= id; i++){
				gc.players[i] = new Player("player");				
			}
		});
		this.socket.on('getOthersId', function(id){
			gc.players[id] = new Player("player");
		});
		this.socket.on('gameStart', function(){
	  		gc.controllEnemySpawning();
	  	});

	  	this.socket.on('posX', function(id, posX){
	  		gc.players[id].setPos(posX);
	  	});
	  	this.socket.on('target', function(id, targetEnemyName){
	  		gc.players[id].setTarget(targetEnemyName);
	  	});
	  	this.socket.on('attack', function(id, damageValue = 100){
	  		gc.players[id].inflictDamageToTargetEnemy(damageValue);
	  	});

	  	this.socket.emit('loaded');
	}
	requestGameStart(){
		this.socket.emit('gameStartReq');
	}
	requestPosX(playerID, posX){
		this.socket.emit('posXReq', playerId, posX);
	}
	requestTarget(playerId, targetEnemyName){
		this.socket.emit('targetReq', playerId, targetEnemyName);
	}
	requestAttack(playerId, damageValue){
		this.socket.emit('attackReq', playerId, damageValue);
	}
}
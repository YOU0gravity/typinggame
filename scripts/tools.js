/*
*
*					|文字判断|
*
*/

//指定された位置に文字を挿入します
function insertStr(str, index, insert){
	return str.slice(0,index) + insert + str.slice(index, str.length);
}
//指定された位置の文字を変更します
function replaceStr(str, index, replace){
	return str.slice(0,index) + replace + str.slice(index+1,str.length);
}

/*
*	文字列中の指定した文字が、タイプされた文字と同じか判断してBooleanで返します
*	@param	{String}	sentence	例文の文字列
*	@param	{Number}	charNum		sentenceから指定する1文字が前から何番目にあるか
*	@param	{String}	typedChar	タイプされた1文字
*	@return {Boolean}
*/
function isSameChar(sentence, charNum, typedChar){
	return sentence[charNum] == typedChar;
}

/*
*	タイプされた文字が、訓令式からヘボン式へ置き換え可能な文字か判断し、Booleanで返します
*	@param	{String}	sentence	例文の文字列
*	@param	{Number}	charNum		sentenceから指定する1文字が前から何番目にあるか
*	@param	{String}	typedChar	タイプされた1文字
*	@return {Boolean}
*/
function canTypeAlterChar(sentence, charNum, typedChar){
	if(sentence){

		switch(typedChar){
			case 'n':
				//TODO:charNum関係を修正
				const reg = /[aiueony]/;
				if(!sentence[charNum].match(reg) && charNum > 1){
					if(sentence[charNum - 1] == 'n' && sentence[charNum - 2] != 'n')
						return true;
				}
				break;

			case 'z':
				if(sentence[charNum] == 'j')
					return true;
				break;

			case 'j':
				if(sentence[charNum] == 'z'){
					if(charNum + 1 <= sentence.length){
						if(sentence[charNum+1] == 'y'){
							return true;
						}		
					}
				}
				break;

			case 'h':
				if(sentence[charNum] == 'f')
					return true;

				if(sentence[charNum] != 'y')
					return false;

				if(charNum > 0 && charNum < sentence.length - 1){
					const reg = /[aiueo]/;
					if(sentence[charNum - 1] == 's' && sentence[charNum + 1].match(reg))
						return true;
				}
				break;

			case 'e':
				if(charNum > 0 && sentence[charNum] == 'h'){
					switch(sentence[charNum - 1]){
						case 't':/*FALLTHROUGH*/
						case 'd':
							return true;
					}
				}
				break;

			case 'l':
				if(sentence[charNum] == 'x')
					return true;
				break;


			case 'f':
				if(charNum + 1 < sentence.length)
					if(sentence[charNum] == 'h' && sentence[charNum + 1] == 'u')
						return true;
				break;

			case 'q'://kuxa qa
				if(sentence[charNum] == 'k'){
					if(charNum + 1 < sentence.length && sentence.substr(charNum + 1, 2) == 'ux'){
						return true;
					}
				}
				break;

			case 's':
				if(charNum != 0){
					if(sentence[charNum - 1] == 't' && sentence[charNum] == 'u'){
						return true;
					}
				}
				break;

			case 'c':
				if(sentence[charNum] == 't' && charNum + 1 < sentence.length && sentence[charNum + 1] == 'i'){
					return true;
				}
				break;
		}

	}else{
		console.log("Error at canTypeAlterChar():Tools.js\nsentenceが空です");
	}

	return false;
}

/*
*	タイプされた文字に応じて、sentenceを加工して返します
*	変換できることは、既に別の関数で確認済みとする
*	@param	{String}	sentence	例文の文字列
*	@param	{Number}	charNum		sentenceから指定する1文字が前から何番目にあるか
*	@param	{String}	typedChar	タイプされた1文字
*	@return {String}
*/
function convertCharToAlter(sentence, charNum, typedChar){
	switch(typedChar){
		case 'n'://次に続く文字が母音・ナ行・拗音ではない場合、'n'を1つ余分に打てる
			//TODO:次の文字の確認？
			sentence = insertStr(sentence, charNum, 'n');
			break;

		case 'z'://replaces "j"
			sentence = replaceStr(sentence,charNum,'z');
			if(charNum+1 <= sentence.length){
				switch(sentence[charNum+1]){
					case "a":case "u":case "e":
					/*FALLTHROUGH*/
					case "o":
						sentence = insertStr(sentence, charNum+1, 'y');
						break;
					default:break;
				}
			}
			break;

		case 'j'://replaces "zy"
			sentence = replaceStr(sentence,charNum,'j');
			sentence = sentence.substr(0,charNum+1) + sentence.substr(charNum+2);
			console.log(sentence);
			break;

		case 'h'://"sy"->"sh", "f"->"h"
			if(sentence[charNum] ==  'f' && charNum + 1 < sentence.length){
				// "fu"->"hu"
				if(sentence[charNum + 1] == 'u')
					sentence = insertStr(sentence, charNum + 1, 'u');
				//"f"+u以外の母音->"hux"
				if(sentence[charNum + 1].match(/[aieo]/))
					sentence = insertStr(sentence, charNum + 1, 'ux');
			}
			sentence = replaceStr(sentence, charNum, 'h');
			break;

		case 'e':
			sentence = replaceStr(sentence, charNum, 'ex');
			break;

		case 'l':
			sentence = replaceStr(sentence, charNum, 'l');
			break;

		case 'f':
			sentence = replaceStr(sentence, charNum, 'f');
			break;

		case 'q':
			sentence = replaceStr(sentence, charNum, 'q');
			sentence = sentence.substr(0, charNum + 1) + sentence.substr(charNum + 3);
			break;

		case 's':
			sentence = insertStr(sentence, charNum, 's');
			break;

		case 'c':
			sentence = replaceStr(sentence, charNum, 'c');
			sentence = insertStr(sentence, charNum + 1, 'h');

	}
	return sentence;
}

/*
*	文字列から後方2文字の"px"を取り除き、数値にして返します
*	ex."400px"{String}を400{Number}にして返す
*
*	!!要修正:形式が正しくない場合、-1を返す
*
*	@param	{string}	str
*	@return	{Number}
*/
function removePx(str){
	if(str.length > 2){
		str = str.substr(0, str.length - 2);
		var num = parseInt(str);
		if(num != NaN){
			return num;
		}
	}
	var errorMsg = 
		"removePx()を実行しましたが、正しく処理されませんでした\n" +
		"渡された引数:" + str;
	console.log(errorMsg);
	return -1
}

function insertTagAtRegularIntervals(str, tag, interval){
	if(!str || !tag || !interval)
		return "";

	let lines = str.split(tag);

	for(let i=0; i<lines.length; i++){
		if(lines[i].length > interval){
			let insertPoint = interval;
			while(insertPoint < lines[i].length){
				lines[i] = insertStr(lines[i], insertPoint, tag);
				insertPoint += interval + tag.length;
			}
		}
	}

	let result = "";
	for(let i=0; i<lines.length - 1;i++){
		result += lines[i] + "<br>";
	}
	result += lines[lines.length - 1];

	return result;
}


/*	
*	とある文字列があり、それにinterval{Number}おきにtag{String}を挿入する場合、
*	挿入する前のcount{Number}番目の文字は、挿入後何番目になるのかを返す
*
*	(ex.ある文字列があり、それに10文字ごとに<br>という文字を挿入した。
*			文字を挿入する前の12番目の文字は、挿入後の16番目の文字となる)
*/
function computeCountWithStrInsertedAtRegularIntervals(count, tag, interval){
	if(!count || !tag || !interval)
		return "";

	count = count + tag.length * Math.floor(count / interval);

	return count;
}

function countSearchingWord(sentence, searchingWord){
	const lines = sentence.split(searchingWord);
	return lines.length;
}




/*
*
*					|ゲーム関連|
*
*/






/*
*	コンボ数と追加効果によってボーナスレートを算出し、それに文字数をかけることで、追加するAPを算出して返す
*	@param {Number} combo	コンボ数
*	@param {Number} length	例文の文字数
*	@param {String} buff	追加効果
*	@return {Number}
*/
function computeAddingAp(combo, length, buff){
	const num = computeBonusRate(combo, buff) * length;
	return num;
}

//baseダメージにコンボボーナスによるダメージを加え、
//optionによる効果によって追加ダメージを加える
//最後に乱数によって値をバラけさせる
function computeDamage(base, combo, option){
	const COMBO_RATE = 0.005;
	var comboBonus = combo * COMBO_RATE;

	var damage = base + comboBonus;
	switch(option){
		case "fever":
			damage = damage * 1.5;
			break;
		default:
			break;
	}

	var rand = Math.random() * 0.2 + 0.9;
	damage = damage * rand;

	return floatFormat(damage, 2);
}

//小数第decPoint位を四捨五入
//TODO:要確認！
function floatFormat(num, decPoint){
	var slide = 1;
	for(var i=0;i<decPoint;i++){
		slide = slide * 10;
	}
	num = Math.round(num * slide) / slide;

	return num;
}



function pickSentenceHavingDesignatedInitial(questionArray, designatedNum = -1){
	let sentenceObj = new Object();

	if(designatedNum != -1){
		if(questionArray[designatedNum].length == 0){
			// const char = convertDecimalNumberToAlphabetChar(designatedNum);
			// console.log("'" + char + "'に対応した配列には文章データがありません");
		}else{
			const rand = Math.floor(Math.random() * questionArray[designatedNum].length);

			sentenceObj.sentenceJp = questionArray[designatedNum][rand][0];
			sentenceObj.sentenceRm = questionArray[designatedNum][rand][1];
		}
	}

	return sentenceObj;
}

//アルファベットが、0から数えて何番目かを返す
function convertAlphabetCharToDecimalNumber(alphabet){
	let num = alphabet.charCodeAt(0);
	if(num >= 65 && num < 65 + 26){//大文字のとき
		num -= 65;
	}else if(num >= 97 && num < 97 + 26){//小文字のとき
		num -= 97;
	}
	return num;
}
function convertDecimalNumberToAlphabetChar(number){
	let alphabet = "";
	if(number > 0 && number < 25)
		alphabet = String.fromCharCode(number + 65);
	return alphabet;
}

function pickNumberOfNotExistInitial(questionArray, existingInitialArray){
	let count = 0;
	let designatedNum = -1;
	let sentenceObj = new Object();

	while(designatedNum == -1){
		const tempNum = Math.floor(Math.random() * questionArray.length);

		if(existingInitialArray[tempNum] == false){
			designatedNum = tempNum;
			break;
		}

		count++;
		if(count > 50)
			break;
	}

	//指定の数以上ランダムに探しても見つからなかった場合、0から順番に探す
	if(designatedNum == -1){
		for(var i=0;i<questionArray.length; i++){
			if(existingInitialArray[i] == false){
				designatedNum = i;
				break;
			}
		}
	}

	return designatedNum;
}

function checkIfTheInitialAlreadyExists(initialArray, initial = ""){
	let doesExist = false;
	if(initial){
		let designatedNum = convertAlphabetCharToDecimalNumber(initial);

		if(initialArray[designatedNum] == true)
			doesExist = true;

	}
	return doesExist;
}

/*
*
*				|その他|
*
*/


/*
*	総移動量、移動にかかる時間、既に経過した時間を使い、
*	経過した時間に応じた移動量を返す
*/
function computeCurrentMoveValue(moveValue, durationMs, passedTimeMs){
	const moveValuePerMs = moveValue / durationMs;
	return moveValuePerMs * passedTimeMs;
}
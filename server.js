var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);



app.get('/', function(req, res){
  res.sendfile('index.html');
});

var count = 0;
var isPlaying = false;
io.on('connection', function(socket){
	console.log('a user connected');

	socket.on('loaded', function(){
		if(isPlaying)
			return;

		socket.emit('getMyId', count);
		socket.broadcast.emit('getOthersId', count);
		count++;
		isPlaying = true;
	});

	socket.on('gameStartReq', function(){
		if(!isPlaying)
			return;

		io.emit('gameStart');
	});

	socket.on('gameOverReq', function(){
		console.log("Game Over");
		count = 0;
		isPlaying = false;
	});

	//TODO:ブロック単位にする
	socket.on('posXReq', function(id, posX){
		if(typeof(id) != "Number" || id < 0 || id >10 || typeof(poX) != "Number" || posX < 0 || posX > 2000)
			return;
  		socket.broadcast.emit('posX', id, posX);
  	});

  	socket.on('targetReq', function(id, targetEnemyName){
  		if(typeof(id) != "Number" || id < 0 || id >10 || typeof(targetEnemyName) != "String")
  			return;
  		//targetEnemyNameをエスケープ
  		socket.broadcast.emit('target', id, targetEnemyName);
  	});

  	socket.on('attackReq', function(id, damageValue){
  		socket.broadcast.emit('attack', id, damageValue);
  	});
});



http.listen(3000, function(){
  console.log('listening on *:3000');
});